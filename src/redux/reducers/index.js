import { persistCombineReducers } from 'redux-persist';
import Post from './post'
import AsyncStorage from "@react-native-community/async-storage";

const config = {
     key: 'primary',
     storage: AsyncStorage
     }

export default persistCombineReducers(config, {
    Post,
}) 