

// import Container from "./src/Containers";
import React, { Component } from "react";
import { SafeAreaView } from "react-native";
import { Provider } from "react-redux";
import { StackNav } from "./src/navigation/StackNav";
import configureStore from "./src/redux/store";

const store = configureStore();

class App extends Component {
  render() {
    return (
      <>
        <Provider store={store}>
          <SafeAreaView style={{ flex: 1 }}>
            <StackNav />
          </SafeAreaView>
        </Provider>
      </>
    );
  }
};

export default App;